### Quark Drive

Quark Drive is hypothetical device that consists of several engines and produces energy. Task is to optimize energy production. Idea of this project is: first, implementation of sequential solution to problem from dynamic programming field; and second, implement multithreaded solution using POSIX threads.

For more details check QuarkDrive/QuarkDrive.pdf with task description.