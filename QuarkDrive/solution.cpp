#ifndef __PROGTEST__
#include "common.h"
using namespace std;
#endif /* __PROGTEST__ */

#define SAFE_DELETE(x) { if (x) { delete (x); (x) = NULL; } }
#define SAFE_DELETE_ARRAY(x) { if (x) { delete [](x);  (x) = NULL; } }

struct TNode {
    TNode() {
        k = -1;
        left_quark = -1;
        right_quark = -1;
    }
    int k;
    int left_quark;
    int right_quark;
};

struct TSolution {
    TSolution() : numGensProcessed(0) {
        best_setup = new TSetup();
        best_setup->m_Energy = 0;
        best_setup->m_Generator = -1;
        best_setup->m_StartPos = -1;
        best_setup->m_Root = NULL;
        solution_mutex = new pthread_mutex_t();
        pthread_mutex_init(solution_mutex, NULL);
    }
    ~TSolution() {
        pthread_mutex_destroy(solution_mutex);
        SAFE_DELETE(solution_mutex);
        SAFE_DELETE(best_setup);
    }
    TSetup *best_setup;
    int numGensProcessed;
    pthread_mutex_t *solution_mutex;
};

struct Job {
    Job() : req(NULL), start_gen(0), best_solution(NULL),
    numHighLoadJobsLeft(0), numLowLoadJobsLeft(0) {}
    
    const TRequest *req;
    int start_gen;
    TSolution *best_solution;
    int numHighLoadJobsLeft;
    int numLowLoadJobsLeft;
};

pthread_mutex_t  g_Mtx;
pthread_mutex_t  g_PrintMtx;
#ifndef __PROGTEST__
sem_t *g_Full;
sem_t *g_Free;
#else
sem_t g_Full;
sem_t g_Free;
#endif //__PROGTEST__
int g_NumGens = 0;
int g_NumThreads = 0;
int g_NumHighLoadJobs = 0;
int g_NumLowLoadJobs = 0;
int g_HighLoad = 0;
const TGenerator *g_Generators = NULL;
const TRequest *g_Request = NULL;
const TRequest *(*g_Dispatcher)() = NULL;
void (*g_Engines)(const TRequest*,TSetup*) = NULL;
Job *g_Job = NULL;

void constructSolution(const vector<vector<vector<TNode> > >& S,
                       const vector<vector<vector<uint> > >& X,
                       int start, int end, uint8_t product, CReactNode** node) {
    *node = new CReactNode((uint8_t)product, X[start][end][product]);
    int k = S[start][end][product].k;
    if (k >= 0) {
        constructSolution(S, X, start, k, S[start][end][product].left_quark, &(*node)->m_L);
        constructSolution(S, X, (k + 1) % S.size(), end, S[start][end][product].right_quark, &(*node)->m_R);
    }
}

void freeSetup(CReactNode** node) {
    if (node) {
        if (*node) {
            freeSetup(&((*node)->m_L));
            freeSetup(&((*node)->m_R));
            SAFE_DELETE(*node);
        }
    }
}

void optimizeEnergySeq (const TGenerator* generators, int generatorsNr,
                        const TRequest* request, TSetup* setup )
{
    int nFuel = request->m_FuelNr;
    const uint8_t *fuel = request->m_Fuel;
    setup->m_Root = NULL;
    
    if (nFuel == 1) {
        if (fuel[0] == request->m_FinalProduct) {
            freeSetup(&setup->m_Root);
            setup->m_Root = new CReactNode(fuel[0], 0);
            setup->m_Energy = 0;
            setup->m_StartPos = 0;
            setup->m_Generator = -1;
        }
        return;
    }
    
    uint total_max_energy = 0;
    vector<uint> max_energy(generatorsNr);
    for (int iGen = 0; iGen < generatorsNr; iGen++) {
        const TGenerator *gen = &generators[iGen];
    
        vector<vector<vector<uint> > > X(nFuel);
        vector<vector<vector<TNode> > > S(nFuel);
        for (int i = 0; i < nFuel; i++) {
            X[i].resize(nFuel);
            S[i].resize(nFuel);
            for (int j = 0; j < nFuel; j++) {
                X[i][j].resize(QUARKS_NR);
                S[i][j].resize(QUARKS_NR);
                for (int q = 0; q < QUARKS_NR; q++) {
                    X[i][j][q] = 0;
                }
            }
        }
        
        for (int xlen = 1; xlen < nFuel; xlen++) {
            for (int i = 0; i < nFuel; i++) {
                for (int l = 1; l <= xlen; l++) {
                    int j = (i + l) % nFuel;
                    int prev_j = (i + l - 1) % nFuel;
                    int max_j = (i + xlen) % nFuel;
                    
                    for (int q1 = 0; q1 < QUARKS_NR; q1++) {
                        uint energy1 = X[i][prev_j][q1];
                        int first_quark = q1;
                        if (i == prev_j) {
                            first_quark = (int) fuel[i];
                            q1 = QUARKS_NR;
                        }
                        if (energy1 || i == prev_j) {
                            for (int q2 = 0; q2 < QUARKS_NR; q2++) {
                                uint energy2 = X[j][max_j][q2];
                                int second_quark = q2;
                                if (j == max_j) {
                                    second_quark = (int) fuel[j];
                                    q2 = QUARKS_NR;
                                }
                                if (energy2 || j == max_j) {
                                    for (int q3 = 0; q3 < QUARKS_NR; q3++) {
                                        uint energy3 = gen->m_Energy[first_quark][second_quark][q3];
                                        if (energy3) {
                                            uint energy_sum = energy1 + energy2 + energy3;
                                            if (energy_sum > X[i][max_j][q3]) {
                                                X[i][max_j][q3] = energy_sum;
                                                S[i][max_j][q3].k = prev_j;
                                                S[i][max_j][q3].left_quark = first_quark;
                                                S[i][max_j][q3].right_quark = second_quark;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        max_energy[iGen] = 0;
        int max_i = 0;
        int max_j = nFuel - 1;
        for (int i = 0; i < nFuel; i++) {
            int j = (i + nFuel - 1) % nFuel;
            if (max_energy[iGen] < X[i][j][request->m_FinalProduct]) {
                max_energy[iGen] = X[i][j][request->m_FinalProduct];
                max_i = i;
                max_j = j;
            }
        }
        if (total_max_energy < max_energy[iGen]) {
            total_max_energy = max_energy[iGen];
            freeSetup(&setup->m_Root);
            constructSolution(S, X, max_i, max_j, request->m_FinalProduct, &setup->m_Root);
            setup->m_Energy = total_max_energy;
            setup->m_Generator = iGen;
            setup->m_StartPos = max_i;
        }
    }

    return;
}

void* workerThread(void* p_job) {
#ifndef __PROGTEST__
    sem_wait(g_Full);
#else
    sem_wait(&g_Full);
#endif //__PROGTEST__
    
    const TRequest *req = NULL;
    TSolution    *best_solution = NULL;
    TSetup *best_setup = NULL;
    int start_gen = -1;
    int num_gens = -1;
    
    while (true) {
        //Read current available job
        pthread_mutex_lock(&g_Mtx);
            //If no jobs are left, read next fuel ring
        if (g_Job->numHighLoadJobsLeft <= 0 && g_Job->numLowLoadJobsLeft <= 0) {
            g_Job->req = g_Dispatcher();
            if (g_Job->req != NULL) {
                g_Job->numHighLoadJobsLeft = g_NumHighLoadJobs;
                g_Job->numLowLoadJobsLeft = g_NumLowLoadJobs;
                g_Job->best_solution = new TSolution();
                g_Job->start_gen = 0;
            } else {
                pthread_mutex_unlock(&g_Mtx);
                break; //we are done!
            }
        }
        start_gen = g_Job->start_gen;
        best_solution = g_Job->best_solution;
        best_setup = best_solution->best_setup;
        req = g_Job->req;
            //Now we definitely should have jobs
        if (g_Job->numHighLoadJobsLeft > 0) {
            num_gens = g_HighLoad;
            g_Job->start_gen += num_gens;
            g_Job->numHighLoadJobsLeft--;
        } else {
            if (g_Job->numLowLoadJobsLeft > 0) {
                num_gens = g_HighLoad - 1;
                g_Job->start_gen += num_gens;
                g_Job->numLowLoadJobsLeft--;
            }
        }
        pthread_mutex_unlock(&g_Mtx);
        
        //Calculate local optimum
        TSetup solution;
        optimizeEnergySeq(&g_Generators[start_gen], num_gens, req, &solution);
        
        //Update global optimum(if needed)
        bool lastWorkingThread = false;
        //pthread_mutex_lock(&g_Mtx);
        pthread_mutex_lock(best_solution->solution_mutex);
        if (solution.m_Root != NULL && solution.m_Energy > best_setup->m_Energy) {
            freeSetup(&best_setup->m_Root);
            best_setup->m_Energy = solution.m_Energy;
            best_setup->m_Generator = start_gen + solution.m_Generator;
            best_setup->m_StartPos = solution.m_StartPos;
            best_setup->m_Root = solution.m_Root;
        }
        best_solution->numGensProcessed += num_gens;
        if (best_solution->numGensProcessed == g_NumGens) {
            lastWorkingThread = true;
        }
        //pthread_mutex_unlock(&g_Mtx);
        pthread_mutex_unlock(best_solution->solution_mutex);
        if (lastWorkingThread) {
#ifndef __PROGTEST__
            pthread_mutex_lock(&g_PrintMtx);
#endif
            g_Engines(req, best_setup);
#ifndef __PROGTEST__
            pthread_mutex_unlock(&g_PrintMtx);
#endif
            SAFE_DELETE(best_solution);
        }
    }
#ifndef __PROGTEST__
    sem_post(g_Free);
#else
    sem_post(&g_Free);
#endif //__PROGTEST__
    
    return NULL;
}

void optimizeEnergy(int threads, const TGenerator* generators,
                    int generatorsNr, const TRequest *(* dispatcher)(),
                    void (* engines ) ( const TRequest *, TSetup * ))
{
    g_NumGens = generatorsNr;
    g_NumThreads = threads;
    if (g_NumThreads < g_NumGens) {
        g_NumHighLoadJobs = g_NumGens % g_NumThreads;
        g_HighLoad = g_NumGens / g_NumThreads + 1;
        g_NumLowLoadJobs = g_NumThreads - g_NumHighLoadJobs;
    } else {
        g_NumHighLoadJobs = 0;
        g_HighLoad = 2;
        g_NumLowLoadJobs = g_NumGens - g_NumHighLoadJobs;
    }
    g_Generators = generators;
    g_Dispatcher = dispatcher;
    g_Engines = engines;
    g_Job = new Job();
    
    vector<pthread_t> thrID(g_NumThreads);
    
    pthread_attr_t  attr;   
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    
    pthread_mutex_init(&g_Mtx, NULL);
    pthread_mutex_init(&g_PrintMtx, NULL);
#ifndef __PROGTEST__
    sem_unlink("/semfree");
    if ((g_Free = sem_open("/semfree", O_CREAT, 0644, g_NumThreads)) == SEM_FAILED) {
        fprintf(stderr, "Free sem creation error\n");
        sem_close(g_Free);
        sem_unlink("/semfree");
        return;
    }
    sem_unlink("/semfull");
    if ((g_Full = sem_open("/semfull", O_CREAT, 0644, 0)) == SEM_FAILED) {
        fprintf(stderr, "Full sem creation error\n");
        sem_close(g_Full);
        sem_unlink("/semfull");
        return;
    }
#else
    sem_init(&g_Free, 0, g_NumThreads);
    sem_init(&g_Full, 0, 0);
#endif //__PROGTEST__
    
    for (int iThr = 0; iThr < g_NumThreads; iThr++) {
#ifndef __PROGTEST__
        sem_wait(g_Free);
#else
        sem_wait(&g_Free);
#endif //__PROGTEST__
        if (pthread_create(&thrID[iThr], &attr, (void*(*)(void*)) workerThread, NULL)) {
            perror("pthread_create error\n");
            goto exit;
        }
#ifndef __PROGTEST__
        sem_post(g_Full);
#else
        sem_post(&g_Full);
#endif //__PROGTEST__
    }
    for (int iThr = 0; iThr < g_NumThreads; iThr++) {
        pthread_join(thrID[iThr], NULL);
    }
    
exit:
    SAFE_DELETE(g_Job);
#ifndef __PROGTEST__
    if (sem_close(g_Free) == -1) {
        fprintf(stderr, "Free sem closing error\n");
    }
    if (sem_unlink("/semfree") == -1) {
        fprintf(stderr, "Free sem unlinking error\n");
    }
    if (sem_close(g_Full) == -1) {
        fprintf(stderr, "Full sem closing error\n");
    }
    if (sem_unlink("/semfull") == -1) {
        fprintf(stderr, "Full sem unlinking error\n");
    }
#else //__PROGTEST__
    sem_destroy(&g_Free);
    sem_destroy(&g_Full);
#endif //__PROGTEST__
    pthread_mutex_destroy(&g_PrintMtx);
    pthread_mutex_destroy(&g_Mtx);
}
