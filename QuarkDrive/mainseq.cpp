#include "common.h"
#include <fstream>
using namespace std;

int main (int argc, char *argv[]) {
    
    fstream inFile("data/test4.txt", ios::in);
    if (!inFile) {
        cerr << "File not found!\n";
        return 1;
    }
    inFile.seekg(0);
 
    vector<TGenerator> generators;
    while (readGenerator(inFile, generators)) {}
    if (!inFile.good()) {
        cout << "Invalid input." << endl;
        return 1;
    }
    
    vector<uint8_t> fuel;
    uint8_t finalProduct;
   
    while (readFuel(inFile, finalProduct, fuel)) {
        TRequest    req;
        TSetup      setup;
        
        req.m_Fuel = &*fuel.begin();
        req.m_FuelNr = fuel.size();
        req.m_FinalProduct = finalProduct;
        
        optimizeEnergySeq(&*generators.begin(), generators.size(), &req, &setup );
        
        cout << "Product " << (int) req.m_FinalProduct << " " << (setup.m_Root ? "Ok" : "N/A" ) << endl;
        if ( setup . m_Root ) {
            cout << "\tGenerator: " << setup . m_Generator << "\n"
            << "\tEnergy: " << setup . m_Energy << "\n"
            << "\tStart pos: " << setup . m_StartPos << endl;
            
            setup.m_Root->PrintTree(cout, setup.m_StartPos, fuel.size());
        }
      
        delete setup.m_Root;
        fuel.resize(0);
    }
    
    inFile.close();

    return 0;
}
